/*
 * filters.c
 *
 * Created: 23.09.2020 14:22:36
 *  Author: ivar.holand
 */

// Cool filter tool: https://www-users.cs.york.ac.uk/~fisher/mkfilter/trad.html

// 20 - 200hz Single Pole Bandpass IIR Filter fs=5461Hz
float bassFilter(float sample)
{
	static float xv[3] = {0,0,0}, yv[3] = {0,0,0};
	xv[0] = xv[1]; xv[1] = xv[2];
	xv[2] = (sample) / 3.f; // change here to values close to 2, to adapt for stronger or weaker sources of line level audio


	yv[0] = yv[1]; yv[1] = yv[2];
	yv[2] = (xv[2] - xv[0])	+ (-0.8117226539f * yv[0]) + (1.8069109216f * yv[1]);
	return yv[2];
}


// 85 - 255hz Single Pole Bandpass IIR Filter fs=5461Hz
float vocalFilter(float sample)
{
	static float xv[3] = {0,0,0}, yv[3] = {0,0,0};
	xv[0] = xv[1]; xv[1] = xv[2];
	xv[2] = (sample) / 3.f; // change here to values close to 2, to adapt for stronger or weaker sources of line level audio


	yv[0] = yv[1]; yv[1] = yv[2];
	yv[2] = (xv[2] - xv[0])	+ (-0.8213107818f * yv[0]) + (1.7951604693f * yv[1]);
	return yv[2];
}


// 10hz Single Pole Low pass IIR Filter fs=5461Hz
float envelopeFilter(float sample)
{ //10hz low pass
	static float xv[2] = {0,0}, yv[2] = {0,0};
	xv[0] = xv[1];
	xv[1] = sample / 60.f;
	yv[0] = yv[1];
	yv[1] = (xv[0] + xv[1]) + (0.9885601267f * yv[0]);
	return yv[1];
}

// 1.7 - 3.0hz Single Pole Bandpass IIR Filter, fs=30Hz
float beatFilter(float sample)
{
	static float xv[3] = {0,0,0}, yv[3] = {0,0,0};
	xv[0] = xv[1]; xv[1] = xv[2];
	xv[2] = sample / 3.7f;
	yv[0] = yv[1]; yv[1] = yv[2];
	yv[2] = (xv[2] - xv[0])
	+ (-0.7590413130f * yv[0]) + (1.5647262787f * yv[1]);
	return yv[2];
}
