/*
 * filters.h
 *
 * Created: 23.09.2020 14:22:25
 *  Author: ivar.holand
 */


#ifndef FILTERS_H_
#define FILTERS_H_

float bassFilter(float sample);
float vocalFilter(float sample);
float envelopeFilter(float sample);
float beatFilter(float sample);

#endif /* FILTERS_H_ */