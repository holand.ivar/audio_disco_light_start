#include <atmel_start.h>
#include <stdbool.h>
#include <stdint.h>
#include "neopixel.h"
#include <util/atomic.h>
#include "filters.h"

enum modes {
	VUMETER,
	TRIPPEL_PEAKS,
	VU_FROM_CENTER,
	FIRE,
	MODES_LAST,
	STROBE,
	ALL_LEDS,
	STARMAP,
};

#define SWITCH_MODE_ON_BPM

volatile bool result_ready = false;
int16_t result = 0;

#define PIXEL_COUNT 107

void adc_result_ready(void);
void adc_result_ready(void)
{
	//PORTF.OUTTGL = PIN5_bm;

	if (result_ready == true) {
		asm volatile ("nop");
		//PORTB.OUTCLR = PIN5_bm;
	}

	result = ADC_0_get_conversion_result();
	result_ready = true;
}

float sample;
float bass_sample;
float envelope_sample;
float beat_sample;

uint16_t ticks = 0;

color_t colors[] = {
	{.r = 255, .g = 0, .b = 0},
	{.r = 0, .g = 0, .b = 255},
	{.r = 0, .g = 255, .b = 0},
	{.r = 255, .g = 255, .b = 255},
	{.r = 255, .g = 128, .b = 0},
	{.r = 255, .g = 0, .b = 255},
	{.r = 0xe6, .g = 0x67, .b = 0xff},
	{.r = 0x1f, .g = 0xff, .b = 0xfb},
};


color_t neopixels[PIXEL_COUNT] = {0};

color_t Wheel(uint8_t i);
color_t WheelCenter(uint8_t i);

float vu_max = 0;
float amplifier = 1.5f;

bool beat_detected = false;

// Vizualizers
void Glow(int initial, bool glow_all, bool beat_detected);
void PeakVisualizer(float envelope, bool beat_detected);
void StarMap(float envelope, bool beat_detected);
void GlowFromCenter(float vu);
void FireEffect(float envelope);
void Stroboscope(void);

uint32_t average_value = 0;
uint16_t accumulate_samples = 0;

uint16_t beat_count = 0;
uint8_t seconds_past = 0;

uint16_t ticks_s = 0;
uint16_t ticks_30_fps = 0;
uint16_t ticks_fall = 0;

enum modes mode = STARMAP;

int main(void)
{
	/* Initializes MCU, drivers and middleware */
	atmel_start_init();

	PORTB.DIRSET = PIN5_bm;
	PORTA.DIRSET = PIN5_bm;

	//SampleFilter_init(&filter);

	LED_Init();
	LED_Fill_LED_RGB_String(RED, PIXEL_COUNT);

	ADC_0_register_callback(adc_result_ready);

	sei();

	/* Replace with your application code */
	while (1) {
		if (result_ready) {

			ticks_s++;
			ticks_30_fps++;
			ticks_fall++;

			sample = result - 512.f;
			sample *= amplifier;

			bass_sample = bassFilter(sample);

			if (bass_sample < 0) {
				bass_sample = -bass_sample;
			}

			envelope_sample = envelopeFilter(bass_sample);

			if (envelope_sample < 0) {
				asm volatile ("nop");
			}

			average_value += envelope_sample;
			accumulate_samples++;

			if (accumulate_samples == 16384) {
				accumulate_samples = 0;

				average_value = average_value/16384;

				if (average_value < PIXEL_COUNT/2) {
					amplifier += 0.1f;
				} else {
					amplifier -= 0.1f;
				}

				if (amplifier >= 4) {
					amplifier = 4;
				} else if (amplifier < 0.1) {
					amplifier = 0.1;
				}
			}

			//PORTB.OUTSET = PIN5_bm;
			result_ready = false;

			ticks++;

		}

		if (ticks_30_fps == 182) {
			// 30fps

			ticks_30_fps = 0;

			if (envelope_sample > vu_max) {
				vu_max = envelope_sample;
			}

			beat_sample = beatFilter(bass_sample);

			if (beat_sample > 10) {
				//beat_count++;
				PORTB.OUTCLR = PIN5_bm;
				if (!beat_detected) {
					beat_count++;
				}
				beat_detected = true;
			} else {
				PORTB.OUTSET = PIN5_bm;
				beat_detected = false;
			}

			switch (mode) {
				case STARMAP:
					StarMap(envelope_sample, beat_detected);
					break;
				case VUMETER:
					Glow(envelope_sample, false, false);
					PeakVisualizer(vu_max, beat_detected);
					break;
				case VU_FROM_CENTER:
					GlowFromCenter(envelope_sample/2);
					break;
				case TRIPPEL_PEAKS:
					Glow(0, false, false);
					PeakVisualizer(vu_max, beat_detected);
					PeakVisualizer(vu_max*2/3, beat_detected);
					PeakVisualizer(vu_max/3, beat_detected);
					break;
				case ALL_LEDS:
					Glow(PIXEL_COUNT, true, beat_detected);
					break;
				case FIRE:
					FireEffect(envelope_sample);
					break;
				case STROBE:
					Stroboscope();
					break;
				default:
					break;
			}

			ATOMIC_BLOCK(ATOMIC_RESTORESTATE) {
				LED_Write_LED_RGB_String((uint24_t *)neopixels, PIXEL_COUNT);
			}

		}

		if (ticks_fall == 100) {
			ticks_fall = 0;
			//22.75 fps
			if (vu_max != 0) {
				vu_max--;
			}
		}

#ifdef SWITCH_MODE_ON_BPM
		if (ticks_s == 5461) {
			seconds_past++;
			ticks_s = 0;

			if (seconds_past >= 10) {
				enum modes prev_mode = mode;

				seconds_past = 0;
				beat_count *= 6;

				if ((beat_count) < 30) {
					mode = STARMAP;
				} else if (beat_count > 200) {
					mode = STROBE;
				} else if (beat_count > 150) {
					mode = ALL_LEDS;
				} else if ((beat_count) > 80) {
					mode = (enum modes)rand() % (uint8_t)MODES_LAST;
				}

				if (mode != prev_mode) {
					Glow(0, false, false);
				}

				beat_count = 0;
			}
		}
#endif
	}
}

#define DIM_SPEED 15

void Glow(int initial, bool glow_all, bool beat_detected)//function to glow LEDs
{
	static color_t color = {.channel = 0};

	if (glow_all) {
		if (beat_detected) {
			color = colors[rand() % (sizeof(colors)/3)];
		} else {
			if (color.r - DIM_SPEED > 0) {
				color.r -= DIM_SPEED;
			} else {
				color.r = 0;
			}
			if (color.g - DIM_SPEED > 0) {
				color.g -= DIM_SPEED;
			} else {
				color.g = 0;
			}
			if (color.b - DIM_SPEED > 0) {
				color.b -= DIM_SPEED;
			} else {
				color.b = 0;
			}
		}
	}

	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		if (i < initial) {
			if (glow_all == false) {
				neopixels[i] = Wheel(i);
			} else {
				neopixels[i] = color;
			}
		} else {
			neopixels[i].channel = 0;
		}
	}
}

void PeakVisualizer(float envlope, bool beat_detected)
{
	static color_t peak_color = {.channel = 0};

	if (envlope > PIXEL_COUNT*1.5) {
		envlope = PIXEL_COUNT*1.5;
	}

	uint8_t pos = (uint8_t)envlope;

	if (beat_detected) {
		peak_color = colors[rand() % (sizeof(colors)/3)];
	}

	if (pos > 2 && pos < PIXEL_COUNT-2) {

		neopixels[pos-2] = peak_color;
		neopixels[pos-1] = peak_color;
		neopixels[pos] = peak_color;
		neopixels[pos+1] = peak_color;
		neopixels[pos+2] = peak_color;
	}
}

void StarMap(float envelope, bool beat_detected)
{
	int16_t dim = 0;

	uint8_t background_brightness = (envelope)/100.0 * 10 + 1;

	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		if (neopixels[i].r > 0) {
			dim = rand() % 10;

			if ((neopixels[i].r - dim) <= 0) {
				neopixels[i].r = 0;
				neopixels[i].g = 0;
				neopixels[i].b = 0;
			} else {
				neopixels[i].r -= dim;
				neopixels[i].g -= dim;
				neopixels[i].b -= dim;

			}
		}

		if (neopixels[i].r == 0) {
			neopixels[i].b = background_brightness;
		}
	}

	if (beat_detected) {
		for (uint8_t i = 0; i < 3; i++) {
			uint8_t brightness = rand() % 255;
			uint8_t pixel = rand()%PIXEL_COUNT;
			neopixels[pixel].r = brightness;
			neopixels[pixel].g = brightness;
			neopixels[pixel].b = brightness;
		}
	}
}

void GlowFromCenter(float vu)
{
	for (uint8_t i = 0; i < PIXEL_COUNT; i++) {
		if (i < (53-vu/2) || i > (53+vu/2)) {
			//strip.setPixelColor(i, 0);
			neopixels[i].channel = 0;
		}	else {
			//strip.setPixelColor(i,Wheel(abs(i-56)));
			neopixels[i] = WheelCenter(abs(i-53));
		}
	}
}

void FireEffect(float envelope)
{
	int r = 226;
	int g = 40;//r-200;
	int b = 2;

	float background_brightness = (envelope)/PIXEL_COUNT;

	int flicker;
	int r1, g1, b1;

	for(int x = 0; x < PIXEL_COUNT; x++) {
		flicker = rand() % 55;
		r1 = r-flicker;
		g1 = g-flicker;
		b1 = b-flicker;

		if(g1<0) {
			g1=0;
		}

		if(r1<0){
			r1=0;
		}

		if(b1<0){
			b1=0;
		}


		color_t fire_color = {.r = r1*background_brightness, .g = g1*background_brightness, .b = b1*background_brightness};

		neopixels[x] = fire_color;
	}
}

void Stroboscope(void)
{
	static color_t strobe = {.channel = 0};

	if (strobe.channel == 0) {
		strobe.channel = 0xffffff;
	} else {
		strobe.channel = 0;
	}

	for (uint8_t a = 0; a < PIXEL_COUNT; a++) {
		neopixels[a] = strobe;
	}
}

static uint8_t max_a_trunk(int16_t val1, int16_t val2)
{
	int16_t retval;

	if (val1 > val2) {
		retval = val1;
	} else {
		retval = val2;
	}

	if (retval > 255) {
		return 255;
	}

	return (uint8_t)retval;
}

color_t Wheel(uint8_t i)
{
	color_t col;
	col.r = max_a_trunk(i*3.86f-127, 0);
	col.g = max_a_trunk(i*-3.45f+255, 0);
	col.b = 0;

	return col;
}

color_t WheelCenter(uint8_t i)
{
	color_t col;
	col.r = max_a_trunk(i*3.86f-32, 0);
	col.g = max_a_trunk(i*-3.45f+64, 0);
	col.b = 0;

	return col;
}
